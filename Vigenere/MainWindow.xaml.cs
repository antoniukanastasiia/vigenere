﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.IO;
using System.IO.Packaging;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using Word = DocumentFormat.OpenXml.Wordprocessing;
using System.Xml.Linq;
using System.Linq;

namespace Vigenere
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void openCipher_Click(object sender, RoutedEventArgs e)
        {
            if (!OpenDocument(textToCipher))
            {
                MessageBox.Show("Ошибка при открытии файла для шифрования", "Невозможно открыть файл", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                textToDecipher.Document.Blocks.Clear();
            }

        }


        private void openDecipher_Click(object sender, RoutedEventArgs e)
        {
            if (!OpenDocument(textToDecipher))
            {
                MessageBox.Show("Ошибка при открытии файла для дешифрования", "Невозможно открыть файл", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            else
            {
                textToCipher.Document.Blocks.Clear();
            }
        }

        private void cipher_Click(object sender, RoutedEventArgs e)
        {
            if (new TextRange(textToCipher.Document.ContentStart, textToCipher.Document.ContentEnd).IsEmpty)
            {
                MessageBox.Show("Невозможно зашифровать текст. Отсутствует текст для шифрования", "Шифрование невозможно", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            string key = cipherKey.Text;
            if (key=="")
            {
                MessageBox.Show("Пожалуйста, введите ключ шифрования", "Требуется ключ", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            VigenereCipher cipher = new VigenereCipher();
            string originalText = new TextRange(textToCipher.Document.ContentStart, textToCipher.Document.ContentEnd).Text;
            string encryptedText = cipher.Encrypt(originalText, key);
            TextRange targetRange = new TextRange(textToDecipher.Document.ContentStart, textToDecipher.Document.ContentEnd);
            Stream outputStream = new MemoryStream();
            byte[] encryptedBytes = System.Text.Encoding.UTF8.GetBytes(encryptedText);
            outputStream.Write(encryptedBytes, 0, encryptedBytes.Length);
            targetRange.Load(outputStream, DataFormats.Text);
            outputStream.Close();
        }

        private void decipher_Click(object sender, RoutedEventArgs e)
        {
            // проверяем, есть ли текст в документе
            if (new TextRange(textToDecipher.Document.ContentStart, textToDecipher.Document.ContentEnd).IsEmpty)
            {
                MessageBox.Show("Невозможно дешифровать текст. Отсутствует текст для дешифрования", "Дешифрование невозможно", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            string key = decipherKey.Text;
            if (key == "")
            {
                MessageBox.Show("Пожалуйста, введите ключ дешифрования", "Требуется ключ", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            VigenereCipher decipher = new VigenereCipher();
            string originalText = new TextRange(textToDecipher.Document.ContentStart, textToDecipher.Document.ContentEnd).Text;
            string decryptedText = decipher.Decrypt(originalText, key);
            TextRange targetRange = new TextRange(textToCipher.Document.ContentStart, textToCipher.Document.ContentEnd);
            Stream outputStream = new MemoryStream();
            byte[] decryptedBytes = System.Text.Encoding.UTF8.GetBytes(decryptedText);
            outputStream.Write(decryptedBytes, 0, decryptedBytes.Length);
            targetRange.Load(outputStream, DataFormats.Text);
            outputStream.Close();
        }

        private void saveCipher_Click(object sender, RoutedEventArgs e)
        {
            // проверяем, есть ли текст в документе
            if (new TextRange(textToDecipher.Document.ContentStart, textToDecipher.Document.ContentEnd).IsEmpty)
            {
                MessageBox.Show("Невозможно сохранить файл. Поле с зашифрованной информацией пусто.", "Сохранение невозможно", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (SaveResult("cipher_result", textToDecipher))
            {
                MessageBox.Show("Файл с зашифрованной информацией был успешно сохранен", "Сохранение успешно", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("Файл с зашифрованной информацией не был сохранен", "Ошибка при сохранении", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void saveDecipher_Click(object sender, RoutedEventArgs e)
        {
            // проверяем, есть ли текст в документе
            if (new TextRange(textToCipher.Document.ContentStart, textToCipher.Document.ContentEnd).IsEmpty)
            {
                MessageBox.Show("Невозможно сохранить файл. Поле с расшифрованной информацией пусто.", "Сохранение невозможно", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }

            if (SaveResult("decipher_result", textToCipher))
            {
                MessageBox.Show("Файл с расшифрованной информацией был успешно сохранен", "Сохранение успешно", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            else
            {
                MessageBox.Show("Файл с расшифрованной информацией не был сохранен", "Ошибка при сохранении", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


        private void exit_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Вы действительно хотите выйти из программы?", "Подтверждение выхода", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
            {
                Environment.Exit(0);
            }
        }


        private bool SaveResult(string fileName, RichTextBox resultBox)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = fileName;
            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text document (.txt)|*.txt|Word documents (.docx)|*.docx";

            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                fileName = dlg.FileName;
                string ext = System.IO.Path.GetExtension(fileName);
                TextRange range = new TextRange(resultBox.Document.ContentStart, resultBox.Document.ContentEnd);
                if (ext == ".txt")
                {
                    using (FileStream fs = new FileStream(fileName, FileMode.Create, FileAccess.Write))
                    {
                        range.Save(fs, DataFormats.Text);
                    }
                    return true;
                }
                else if (ext == ".docx")
                {
                    using (WordprocessingDocument document = WordprocessingDocument.Create(fileName, WordprocessingDocumentType.Document))
                    {
                        MainDocumentPart mainPart = document.AddMainDocumentPart();
                        mainPart.Document = new Word.Document(new Word.Body());

                        // получаем текст из FlowDocument и сохраняем его в docx
                        foreach (Block block in resultBox.Document.Blocks)
                        {
                            Paragraph p = (Paragraph)block;
                            string originalRunText = ((Run)p.Inlines.FirstInline).Text;
                            Word.Paragraph para = mainPart.Document.Body.AppendChild(new Word.Paragraph());
                            Word.Run run = para.AppendChild(new Word.Run());
                            run.AppendChild(new Word.Text(originalRunText));
                        }
                        return true;
                    }

                }
                else
                {
                    return false;
                }

            }
            return false;
        }

        private bool OpenDocument (RichTextBox targetBox)
        {
            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();

            dlg.DefaultExt = ".txt";
            dlg.Filter = "Text documents (.txt)|*.txt|Word documents (.docx)|*.docx";
            Nullable<bool> result = dlg.ShowDialog();

            if (result == true)
            {
                string fileName = dlg.FileName;
                string ext = System.IO.Path.GetExtension(fileName);
                TextRange range = new TextRange(targetBox.Document.ContentStart, targetBox.Document.ContentEnd);

                if (ext == ".txt")
                {
                    FileStream fStream = new FileStream(fileName, FileMode.OpenOrCreate);
                    range.Load(fStream, DataFormats.Text);
                    fStream.Close();
                    return true;
                }
                else if (ext == ".docx")
                {
                    // открываем архив с документом Word, находим основной XML файл и парсим его параграфы
                    Package package = Package.Open(fileName);
                    Uri documentUri = new Uri("/word/document.xml", UriKind.Relative);
                    PackagePart documentPart = package.GetPart(documentUri);
                    XElement wordDoc = XElement.Load(new StreamReader(documentPart.GetStream()));
                    XNamespace w = @"http://schemas.openxmlformats.org/wordprocessingml/2006/main";
                    var paragraphs = from p in wordDoc.Descendants(w + "p")
                                     select p;
                    // добавляем все параграфы в FlowDocument
                    targetBox.Document.Blocks.Clear();
                    foreach (var p in paragraphs)
                    {
                        Paragraph par = new Paragraph();
                        Run r = new Run(p.Value);
                        par.Inlines.Add(r);
                        targetBox.Document.Blocks.Add(par);
                    }
                    return true;
                }
            }
            return false;
        }
    }
}
