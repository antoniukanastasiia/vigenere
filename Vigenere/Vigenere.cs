﻿using System;

namespace Vigenere
{
    class VigenereCipher
    {
        const string alphabet = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
        
        // получаем строку с повторяющимся ключом в соответствии с длиной оригинального текста
        private string GetRepeatedKey(string s, int n)
        {
            var p = s;
            while (p.Length < n)
            {
                p += p;
            }
            return p.Substring(0, n);
        }

        //шифрование или дешифрация текста по алгоритму Виженера
        //параметр encrypting отвечает за переключение между шифрованием и дешифрацией
        string GetVigenereResult(string text, string key, bool encrypting = true)
        {
            string repeatedKey = GetRepeatedKey(key, text.Length);
            string retValue = "";
            int alphLen = alphabet.Length;
            int j = 0;

            for (int i = 0; i < text.Length; i++)
            {
                int letterIndex = alphabet.IndexOf(text[i].ToString().ToLower());
                if (letterIndex < 0)
                {
                    j++;
                    retValue += text[i].ToString();
                }
                else
                {
                    int codeIndex = alphabet.IndexOf(repeatedKey[i-j]);
                    retValue += alphabet[(alphLen + letterIndex + ((encrypting ? 1 : -1) * codeIndex)) % alphLen].ToString();
                }
            }

            return retValue;
        }

        public string Encrypt(string plainMessage, string password)
            => GetVigenereResult(plainMessage, password);

        public string Decrypt(string encryptedMessage, string password)
            => GetVigenereResult(encryptedMessage, password, false);
    }
}

